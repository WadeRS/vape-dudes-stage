<?php
class GrandRiver_PromoMessages_Model_Observer {

    public function hookToControllerActionPreDispatch($observer)
    {
        if (Mage::app()->getStore()->isAdmin()) {
            return;
        }
        $this->savePromo($observer);
        $actionName = $observer->getEvent()->getControllerAction()->getFullActionName();
        switch ($actionName) {
            case 'checkout_cart_index' :
                $this->applyPromo($observer);
                break;
            case 'checkout_onepage_save_order' :
                $this->resetPromo($observer);
                break;
            default:
                return;
        }
    }

    /**
    * Saves Promo Code (promo) to customer session
    * @param   Varien_Event_Observer $observer
    */
    public function savePromo($observer)
    {
        $param = Mage::getStoreConfig('grandriver_promomessages/config/coupon_code_get_var');
        if ($promo = Mage::app()->getFrontController()->getRequest()->getParam($param)) {
            $this->getSession()->setPromoCode($promo);
        }
    }

    public function applyPromo($observer)
    {
        if ($promo = $this->getSession()->getPromoCode()) {
            $this->_applyPromoCode($observer, $promo);
        }
    }

    public function resetPromo()
    {
//        $promo = $this->getSession()->getPromoCode();
//        $onepage = Mage::getSingleton('checkout/type_onepage');
//        $multishipping = Mage::getSingleton('checkout/type_multishipping');
//        if ($onepage->getQuote()->getCouponCode() == $promo || $multishipping->getQuote()->getCouponCode() == $promo) {
            $this->getSession()->setPromoCode(NULL);
//        }
    }

    private function _applyPromoCode($observer, $promo)
    {
        $url = Mage::getSingleton('core/url')->getUrl('*/*/couponPost/coupon_code/'.$promo.'/');
        $this->resetPromo();
        Mage::app()->getResponse()->setRedirect($url);
    }

    /**
     * Get one page checkout model
     *
     * @return Mage_Checkout_Model_Type_Onepage
     */
    public function getOnepage()
    {
        return Mage::getSingleton('checkout/type_onepage');
    }
    
    public function getSession() {
        return Mage::getSingleton('core/session');
    }

}