<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Core
 * @copyright   Copyright (c) 2011 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


/**
 *
 * @category   Bluejalappeno
 * @package    Bluejalappeno_Orderexport
 * @copyright  Copyright (c) 2012 Wimbolt Ltd (http://www.bluejalappeno.com)
 * @license    http://www.bluejalappeno.com/license.txt - Commercial license
 */

class Bluejalappeno_Orderexport_Model_Config_Backend_Export_Cron extends Mage_Core_Model_Config_Data
{

    const CRON_STRING_PATH = 'crontab/jobs/orderexport/schedule/cron_expr';
    const CRON_SUMMARY_PATH = 'crontab/jobs/orderexportsummary/schedule/cron_expr';

    protected function _afterSave()
    {
        $enabled = $this->getData('groups/automatic/fields/cron_enabled/value');
        $cronExprString = '';
        $cronExtraString = '';
        $time = $this->getData('groups/automatic/fields/time/value');
        $frequency = $this->getData('groups/automatic/fields/frequency/value');
		$extraFrequency = $this->getData('groups/automatic/fields/frequency_extra/value');
		$usingExtra = $extraFrequency != Bluejalappeno_Orderexport_Model_Config_Backend_Export_Summaryfrequency::CRON_UNUSED ? true : false;
 
        $cronDayOfWeek = date('N');
        if ($enabled) {
        	if ($frequency == Bluejalappeno_Orderexport_Model_Config_Backend_Export_Frequency::CRON_HOURLY) {
        		$cronExprString = '0 * * * *';
        	}
        	else if ($frequency == Bluejalappeno_Orderexport_Model_Config_Backend_Export_Frequency::CRON_FIVEMINS) {
        		$cronExprString = '*/5 * * * *';
        	}
        	else {
		        $cronExprArray = array(
		            intval($time[1]),                                   # Minute
		            intval($time[0]),                                   # Hour
		            ($frequency == Bluejalappeno_Orderexport_Model_Config_Backend_Export_Frequency::CRON_MONTHLY) ? '1' : '*',       # Day of the Month
		            '*',                                                # Month of the Year
		            ($frequency == Bluejalappeno_Orderexport_Model_Config_Backend_Export_Frequency::CRON_WEEKLY) ? '1' : '*',        # Day of the Week
		        );
		        $cronExprString = join(' ', $cronExprArray);
        	}
        	if($usingExtra) {
        		$cronExtraArray = array(
		            intval($time[1]),                                   # Minute
		            intval($time[0]),                                   # Hour
		            ($extraFrequency == Bluejalappeno_Orderexport_Model_Config_Backend_Export_Summaryfrequency::CRON_MONTHLY) ? '1' : '*',       # Day of the Month
		            '*',                                                # Month of the Year
		            ($extraFrequency == Bluejalappeno_Orderexport_Model_Config_Backend_Export_Summaryfrequency::CRON_WEEKLY) ? '1' : '*',        # Day of the Week
		        );
		        $cronExtraString = join(' ', $cronExtraArray);
	        }
        }
        try {
            Mage::getModel('core/config_data')
                ->load(self::CRON_STRING_PATH, 'path')
                ->setValue($cronExprString)
                ->setPath(self::CRON_STRING_PATH)
                ->save();
            if($usingExtra) {
	            Mage::getModel('core/config_data')
	                ->load(self::CRON_SUMMARY_PATH, 'path')
	                ->setValue($cronExtraString)
	                ->setPath(self::CRON_SUMMARY_PATH)
	                ->save();
            }
        } catch (Exception $e) {
            throw new Exception(Mage::helper('cron')->__('Unable to save the cron expression.'));
        }

    }

 
}
