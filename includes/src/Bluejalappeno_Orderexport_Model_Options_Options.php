<?php

/**
 *
 * @category   Bluejalappeno
 * @package    Bluejalappeno_Orderexport
 * @copyright  Copyright (c) 2012 Wimbolt Ltd (http://www.bluejalappeno.com)
 * @license    http://www.bluejalappeno.com/license.txt - Commercial license
 */

class Bluejalappeno_Orderexport_Model_Options_Options
{

	public function toOptionArray()
    {
        return array(
            array('value' => 'Standard', 'label'=>Mage::helper('adminhtml')->__('Standard')),
            array('value' => 'Sage', 'label'=>Mage::helper('adminhtml')->__('Sage')),
            array('value' => 'Highrise', 'label'=>Mage::helper('adminhtml')->__('Highrise')),
            array('value' => 'VatExempt', 'label'=>Mage::helper('adminhtml')->__('Vat Exempt'))//,
      //      array('value' => 'RMDMO', 'label'=>Mage::helper('adminhtml')->__('Royal Mail Despatch Manager Online'))

        );
    }



}