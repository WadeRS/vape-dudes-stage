<?php
/**
 * @category    GrandRiver
 * @package     GrandRiver_PromoMessages
 * @copyright   Copyright (c) 2012 Grand River, Inc. (http://www.thegrandriver.com)
 * @developer   Marshel Barbash marshel.barbash@thegrandriver.com
 */
class GrandRiver_PromoMessages_Model_Source_Staticblocks
{
    /**
     * Returns all static blocks to populate admin dropdown
     *
     * @return array
     */
    public function toOptionArray()
    {
        $choices = array(
            array(
                'value'=>0,
                'label'=> Mage::helper('grandriver_promomessages')->__('None')
            )
        );
        $blocks = Mage::getModel('cms/block')->getCollection();
        foreach($blocks as $block) {
            $choices[] = array(
                'value'=>$block->getBlockId(),
                'label'=>Mage::helper('grandriver_promomessages')->__($block->getTitle())
            );
        }
        return $choices;
    }

}