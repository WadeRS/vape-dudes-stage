<?php

/**
 * Magestore
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category    Magestore
 * @package     Magestore_RewardPointsCsv
 * @copyright   Copyright (c) 2012 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */

/**
 * Rewardpointscsv Adminhtml Controller
 * 
 * @category    Magestore
 * @package     Magestore_RewardPointsCsv
 * @author      Magestore Developer
 */
class Magestore_RewardPointsCsv_Adminhtml_RewardpointscsvController extends Mage_Adminhtml_Controller_Action {

    /**
     * init layout and set active for current menu
     *
     * @return Magestore_RewardPointsCsv_Adminhtml_RewardpointscsvController
     */
    protected function _initAction() {
        $this->loadLayout()
                ->_setActiveMenu('rewardpoints/rewardpointscsv')
                ->_addBreadcrumb(
                        Mage::helper('adminhtml')->__('RewardPoints Information'), Mage::helper('adminhtml')->__('RewardPoints Information')
        );
        return $this;
    }

    /**
     * index action
     */
    public function indexAction() {
        $this->_initAction()
                ->renderLayout();
    }

    public function importAction() {
        $this->loadLayout()
                ->_setActiveMenu('rewardpoints/rewardpointscsv');
        $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
        $this->_addContent($this->getLayout()->createBlock('rewardpointscsv/adminhtml_rewardpointscsv_import'));
        $this->_title($this->__('Point Balance'))
                ->_title($this->__('Import point balance'));
        $this->renderLayout();
    }

    public function processImportAction() {
        if (isset($_FILES['filecsv']['name']) && $_FILES['filecsv']['name'] != '') {
            try {
                /* Starting upload */
                $uploader = new Varien_File_Uploader('filecsv');

                // Any extention would work
                $uploader->setAllowedExtensions(array('csv'));
                $uploader->setAllowRenameFiles(false);

                // Set the file upload mode 
                // false -> get the file directly in the specified folder
                // true -> get the file in the product like folders 
                //  
                $uploader->setFilesDispersion(false);

                // We set media as the upload dir
//                $path = Mage::getBaseDir('var') . DS . 'tmp' . DS;
//                $result = $uploader->save($path, $_FILES['filecsv']['name']);
                $fileName = $_FILES['filecsv']['tmp_name'];

                if (isset($fileName) && $fileName != '') {
                    $csvObject = new Varien_File_Csv();
                    $dataFile = $csvObject->getData($fileName);
                    $customerData = array();
                    foreach ($dataFile as $row => $cols) {
                        if ($row == 0) {
                            $fields = $cols;
                        } else {
                            $customerData[] = array_combine($fields, $cols);
                        }
                    }
                }
                if (isset($customerData) && count($customerData)) {
                    $cnt = $this->_updateCustomer($customerData);
                    $successMessage = $this->__('Imported total %d customer point balance(s)', $cnt);
                    if ($this->getRequest()->getParam('print')) {
                        $url = $this->getUrl('*/*/massPrint', array(
                            'rewardpointscsv' => implode(',', $customerData)
                        ));
                        $successMessage .= "<script type='text/javascript'>document.observe('dom:loaded',function(){window.location.href = '$url';});</script>";
                    }
                    Mage::getSingleton('adminhtml/session')->addSuccess($successMessage);
                    $this->_redirect('*/*/index');
                    return $this;
                } else {
                    Mage::getSingleton('adminhtml/session')->addError($this->__('Point balance imported'));
                }
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        } else {
            Mage::getSingleton('adminhtml/session')->addError($this->__('No uploaded files'));
        }
        $this->_redirect('*/*/import');
    }

    protected function _updateCustomer($customerData) {

        $collection = array();
        foreach ($customerData as $key => $value) {
            $website = Mage::getSingleton('adminhtml/system_config_source_website')->toOptionArray();
            $website_id = Mage::app()->getDefaultStoreView()->getWebsiteId();
            foreach ($website as $key => $id) {
                if ($id['label'] == $value['Website']) {
                    $website_id = $id['value'];
                    break;
                }
            }
            $email = $value['Email'];
            $pointBalance = $value['Point Balance'];
            $customerExist = $this->_checkCustomer($email, $website_id);
            if (!$customerExist || !$customerExist->getId()) {
                continue;
            }
            $customerExist->setPointBalance($pointBalance);
            $collection[] = $customerExist;
        }
        Mage::getResourceModel('rewardpoints/transaction')->importPointFromCsv($collection);
        return count($collection);
    }

    /**
     * check customer exist by email
     * @param type $email
     * @param type $website_id
     * @return type
     */
    protected function _checkCustomer($email, $website_id = 1) {
        return Mage::getModel('customer/customer')->setWebsiteId($website_id)->loadByEmail($email);
    }
/**
 * download simple csv
 */
    public function downloadSampleAction() {
        $filename = Mage::getBaseDir('media') . DS . 'rewardpointscsv' . DS . 'import_point_balance_sample.csv';
        $this->_prepareDownloadResponse('import_point_balance_sample.csv', file_get_contents($filename));
    }

    /**
     * view and edit item action
     */
//    public function editAction()
//    {
//        $rewardpointscsvId     = $this->getRequest()->getParam('id');
//        $model  = Mage::getModel('rewardpointscsv/rewardpointscsv')->load($rewardpointscsvId);
//
//        if ($model->getId() || $rewardpointscsvId == 0) {
//            $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
//            if (!empty($data)) {
//                $model->setData($data);
//            }
//            Mage::register('rewardpointscsv_data', $model);
//
//            $this->loadLayout();
//            $this->_setActiveMenu('rewardpointscsv/rewardpointscsv');
//
//            $this->_addBreadcrumb(
//                Mage::helper('adminhtml')->__('Item Manager'),
//                Mage::helper('adminhtml')->__('Item Manager')
//            );
//            $this->_addBreadcrumb(
//                Mage::helper('adminhtml')->__('Item News'),
//                Mage::helper('adminhtml')->__('Item News')
//            );
//
//            $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
//            $this->_addContent($this->getLayout()->createBlock('rewardpointscsv/adminhtml_rewardpointscsv_edit'))
//                ->_addLeft($this->getLayout()->createBlock('rewardpointscsv/adminhtml_rewardpointscsv_edit_tabs'));
//
//            $this->renderLayout();
//        } else {
//            Mage::getSingleton('adminhtml/session')->addError(
//                Mage::helper('rewardpointscsv')->__('Item does not exist')
//            );
//            $this->_redirect('*/*/');
//        }
//    }
//    public function newAction()
//    {
//        $this->_forward('edit');
//    }

    /**
     * save item action
     */
//    public function saveAction()
//    {
//        if ($data = $this->getRequest()->getPost()) {
//            if (isset($_FILES['filename']['name']) && $_FILES['filename']['name'] != '') {
//                try {
//                    /* Starting upload */    
//                    $uploader = new Varien_File_Uploader('filename');
//                    
//                    // Any extention would work
//                       $uploader->setAllowedExtensions(array('jpg','jpeg','gif','png'));
//                    $uploader->setAllowRenameFiles(false);
//                    
//                    // Set the file upload mode 
//                    // false -> get the file directly in the specified folder
//                    // true -> get the file in the product like folders 
//                    //    (file.jpg will go in something like /media/f/i/file.jpg)
//                    $uploader->setFilesDispersion(false);
//                            
//                    // We set media as the upload dir
//                    $path = Mage::getBaseDir('media') . DS ;
//                    $result = $uploader->save($path, $_FILES['filename']['name'] );
//                    $data['filename'] = $result['file'];
//                } catch (Exception $e) {
//                    $data['filename'] = $_FILES['filename']['name'];
//                }
//            }
//              
//            $model = Mage::getModel('rewardpointscsv/rewardpointscsv');        
//            $model->setData($data)
//                ->setId($this->getRequest()->getParam('id'));
//            
//            try {
//                if ($model->getCreatedTime == NULL || $model->getUpdateTime() == NULL) {
//                    $model->setCreatedTime(now())
//                        ->setUpdateTime(now());
//                } else {
//                    $model->setUpdateTime(now());
//                }
//                $model->save();
//                Mage::getSingleton('adminhtml/session')->addSuccess(
//                    Mage::helper('rewardpointscsv')->__('Item was successfully saved')
//                );
//                Mage::getSingleton('adminhtml/session')->setFormData(false);
//
//                if ($this->getRequest()->getParam('back')) {
//                    $this->_redirect('*/*/edit', array('id' => $model->getId()));
//                    return;
//                }
//                $this->_redirect('*/*/');
//                return;
//            } catch (Exception $e) {
//                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
//                Mage::getSingleton('adminhtml/session')->setFormData($data);
//                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
//                return;
//            }
//        }
//        Mage::getSingleton('adminhtml/session')->addError(
//            Mage::helper('rewardpointscsv')->__('Unable to find item to save')
//        );
//        $this->_redirect('*/*/');
//    }

    /**
     * delete item action
     */
//    public function deleteAction()
//    {
//        if ($this->getRequest()->getParam('id') > 0) {
//            try {
//                $model = Mage::getModel('rewardpointscsv/rewardpointscsv');
//                $model->setId($this->getRequest()->getParam('id'))
//                    ->delete();
//                Mage::getSingleton('adminhtml/session')->addSuccess(
//                    Mage::helper('adminhtml')->__('Item was successfully deleted')
//                );
//                $this->_redirect('*/*/');
//            } catch (Exception $e) {
//                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
//                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
//            }
//        }
//        $this->_redirect('*/*/');
//    }

    /**
     * mass delete item(s) action
     */
//    public function massDeleteAction()
//    {
//        $rewardpointscsvIds = $this->getRequest()->getParam('rewardpointscsv');
//        if (!is_array($rewardpointscsvIds)) {
//            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s)'));
//        } else {
//            try {
//                foreach ($rewardpointscsvIds as $rewardpointscsvId) {
//                    $rewardpointscsv = Mage::getModel('rewardpointscsv/rewardpointscsv')->load($rewardpointscsvId);
//                    $rewardpointscsv->delete();
//                }
//                Mage::getSingleton('adminhtml/session')->addSuccess(
//                    Mage::helper('adminhtml')->__('Total of %d record(s) were successfully deleted',
//                    count($rewardpointscsvIds))
//                );
//            } catch (Exception $e) {
//                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
//            }
//        }
//        $this->_redirect('*/*/index');
//    }
//    
//    /**
//     * mass change status for item(s) action
//     */
//    public function massStatusAction()
//    {
//        $rewardpointscsvIds = $this->getRequest()->getParam('rewardpointscsv');
//        if (!is_array($rewardpointscsvIds)) {
//            Mage::getSingleton('adminhtml/session')->addError($this->__('Please select item(s)'));
//        } else {
//            try {
//                foreach ($rewardpointscsvIds as $rewardpointscsvId) {
//                    Mage::getSingleton('rewardpointscsv/rewardpointscsv')
//                        ->load($rewardpointscsvId)
//                        ->setStatus($this->getRequest()->getParam('status'))
//                        ->setIsMassupdate(true)
//                        ->save();
//                }
//                $this->_getSession()->addSuccess(
//                    $this->__('Total of %d record(s) were successfully updated', count($rewardpointscsvIds))
//                );
//            } catch (Exception $e) {
//                $this->_getSession()->addError($e->getMessage());
//            }
//        }
//        $this->_redirect('*/*/index');
//    }

    /**
     * export grid item to CSV type
     */
    public function exportCsvAction() {
        $fileName = 'rewardpointscsv.csv';
        $content = $this->getLayout()
                ->createBlock('rewardpointscsv/adminhtml_rewardpointscsv_grid')
                ->getCsv();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * export grid item to XML type
     */
    public function exportXmlAction() {
        $fileName = 'rewardpointscsv.xml';
        $content = $this->getLayout()
                ->createBlock('rewardpointscsv/adminhtml_rewardpointscsv_grid')
                ->getXml();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    protected function _isAllowed() {
        return Mage::getSingleton('admin/session')->isAllowed('rewardpoints/rewardpointscsv');
    }

}