<?php

/**
 *
 * @category   Bluejalappeno
 * @package    Bluejalappeno_Orderexport
 * @copyright  Copyright (c) 2012 Wimbolt Ltd (http://www.bluejalappeno.com)
 * @license    http://www.bluejalappeno.com/license.txt - Commercial license
 */

class Bluejalappeno_Orderexport_Model_Options_Attributes
{

	/**
     * Default ignored attribute codes
     *
     * @var array
     */
    protected $_ignoredAttributeCodes = array(
        'custom_design','custom_design_from','custom_design_to','custom_layout_update',
        'gift_message_available','news_from_date','news_to_date','options_container',
        'price_view','sku_type','image_label', 'thumbnail_label','name', 'status', 'sku' ,'price' ,
        'enable_googlecheckout', 'small_image_label'
    );

    /**
     * Default ignored attribute types
     *
     * @var array
     */
    protected $_ignoredAttributeTypes = array('hidden', 'media_image', 'image', 'gallery');

	public function toOptionArray()
    {
    	$entityTypeId = Mage::getSingleton('eav/config')->getEntityType('catalog_product')->getId();
    	 /* @var $collection Mage_Catalog_Model_Resource_Eav_Mysql4_Product_Attribute_Collection */
        $collection = Mage::getResourceModel('catalog/product_attribute_collection');
        $collection->setEntityTypeFilter($entityTypeId);

        $attributes = array();

        foreach ($collection as $attribute) {
            /* @var $attribute Mage_Eav_Model_Entity_Attribute */
        	if ($this->_isAllowedAttribute($attribute)) {
	            $attributes[] = array(
	                'value' => $attribute->getAttributeCode(),
	                'label' => $attribute->getFrontend()->getLabel(),
	            );
        	}
        }
         array_unshift($attributes, array('value'=>'', 'label'=>Mage::helper('catalog')->__('Please select ...')));
        return $attributes;

    }


 	protected function _isAllowedAttribute($attribute)
    {
        return !in_array($attribute->getFrontendInput(), $this->_ignoredAttributeTypes)
               && !in_array($attribute->getAttributeCode(), $this->_ignoredAttributeCodes)
               && $attribute->getFrontendLabel() != "";
    }
}


