<?php
/**
 * Shopping Cart Editor
 *
 * @category:    Aitoc
 * @package:     Aitoc_Aiteditablecart
 * @version      2.1.8
 * @license:     QJLpuB2aEkjYR94VLCHRHtZ5qCz3k9pHhguxuAjiFY
 * @copyright:   Copyright (c) 2014 AITOC, Inc. (http://www.aitoc.com)
 */
if(Mage::getConfig()->getModuleConfig('Aitoc_Aitoptionstemplate')->is('active', 'true')){
    class Aitoc_Aiteditablecart_Block_Onlyif_Cot extends Aitoc_Aitoptionstemplate_Block_Product_Option_Dependable_Cart
    {
	    public function _construct()
        {
    	    parent::_construct();

            $this->setTemplate('aitoptionstemplate/dependable_cart.phtml');        
        }
    }
}
else{
    class Aitoc_Aiteditablecart_Block_Onlyif_Cot extends Mage_Core_Block_Template
    {

    }
}