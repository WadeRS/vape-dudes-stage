<?php
/**
 * @category    GrandRiver
 * @package     GrandRiver_PromoMessages
 * @copyright   Copyright (c) 2012 Grand River, Inc. (http://www.thegrandriver.com)
 * @developer   Marshel Barbash marshel.barbash@thegrandriver.com
 */
class GrandRiver_PromoMessages_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function getIsEnabled()
    {
        return Mage::getStoreConfigFlag("grandriver_promomessages/config/enabled");
    }

    public function getUseSubtotalWithDiscount()
    {
        return Mage::getStoreConfigFlag("grandriver_promomessages/config/use_discount_for_freeshipping_enabled");
    }
}
