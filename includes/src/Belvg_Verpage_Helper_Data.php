<?php
/**
 * BelVG LLC.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 *
 *******************************************************************
 * @category   Belvg
 * @package    Belvg_Verpage
 * @version    1.0.1
 * @copyright  Copyright (c) 2010 - 2013 BelVG LLC. (http://www.belvg.com)
 * @license    http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 */
class Belvg_Verpage_Helper_Data extends Mage_Core_Helper_Data 
{
    
    public function getUrl($lib)
    {
        switch ($lib) {
            case 'jquery': 
                $version = (array)Mage::getConfig()->getNode('jquery/versions/' . Mage::getStoreConfig('jquery/settings/jq_version'));
                return $version['lib']; 
            case 'noconflict':
                return 'belvg/jquery/jquery.noconflict.js'; 
            default:
                break;
        }
    }
    
}