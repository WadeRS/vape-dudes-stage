<?php
/**
 * Block to display $XX until you receive free shipping
 *
 * @category    GrandRiver
 * @package     GrandRiver_PromoMessages
 * @copyright   Copyright (c) 2012 Grand River, Inc. (http://www.thegrandriver.com)
 * @developer   Marshel Barbash marshel.barbash@thegrandriver.com
 */

class GrandRiver_PromoMessages_Block_Checkout_Cart_Untilfreeshipping extends Mage_Checkout_Block_Cart {

    /**
     * Default text format for $X Until Free Shipping Message
     *
     * @var string
     */
    private $_untilFreeShippingFormat = '<p>Spend <strong>$%s</strong> more to receive <strong>free shipping</strong></p>';

    /**
     * Flag to denote achievement of free shipping
     *
     * @var bool
     */
    private $_isFreeShipping;

    public function __construct()
    {
        $quoteFreeShipping = $this->getQuote()->getShippingAddress()->getFreeShipping();
        $this->setFreeShippingAchieved($quoteFreeShipping);
        parent::__construct();
    }

    /**
     * Config check to see if Free Shipping Messaging is Enabled
     *
     * @return mixed
     */
    public function isEnabled() {
        return Mage::getStoreConfig('grandriver_promomessages/config/freeshipping_messaging_enabled');
    }

    /**
     * Config check to see if Free Shipping Achieved Messaging is enabled
     *
     * @return bool
     */
    public function canShowFreeShippingMessages() {
        return $this->isEnabled() && Mage::getStoreConfig('grandriver_promomessages/config/x_until_freeshipping_messaging_enabled');
    }

    /**
     * Returns content for free shipping messaging
     *
     * @return mixed|string
     */
    public function getXUntilFreeShippingMessage() {
		$freeShippingMsg = '';
        if ($this->canShowFreeShippingMessages()) {
            $msgFormat = Mage::getStoreConfig('grandriver_promomessages/config/x_until_freeshipping_messaging_format');
            if ($msgFormat && preg_match('/%s/', $msgFormat)) {
                $this->_untilFreeShippingFormat = $msgFormat;
            }
            if ($this->getFreeShippingAchieved()) {
                return $this->getFreeShippingAchievedMessage();
            }
            else {
                $shippingMethodUntilFreeShippingAmount = $this->calcFreeShippingMethod();
                $promotionUntilFreeShippingAmount = $this->calcFreeShippingPromotion();
                if ($this->getFreeShippingAchieved()) {
                    $freeShippingMsg = $this->getFreeShippingAchievedMessage();
                }
                else if ($amountLeft = $this->getLowestAmount($shippingMethodUntilFreeShippingAmount, $promotionUntilFreeShippingAmount)) {
                    $freeShippingMsg = sprintf($this->_untilFreeShippingFormat, number_format($amountLeft, 2));
                }
            }
        }
        return $freeShippingMsg;
    }

    /**
     * Returns lowest, not 0, number passed in number
     *
     * @param $x
     * @param $y
     * @return mixed
     */
    public function getLowestAmount($x, $y) {

        if ($x && $y) {
            $result = $x <= $y ? $x : $y;
        }
        else if ($x) {
            $result = $x;
        }
        else if ($y) {
            $result = $y;
        }
        else {
            $result = 0;
        }
        return $result;
    }

    /**
     * Returns configured Free Shipping Achieved Message from
     * either a chosen static block or an config textarea
     *
     * @return mixed|string
     */
    public function getFreeShippingAchievedMessage() {
        $staticBlock = Mage::getStoreConfig('grandriver_promomessages/config/freeshipping_messaging_cms_block');
        $msg = '';
        if ($this->isEnabled()) {
            $msg = $staticBlock ?
                $this->getLayout()->createBlock('cms/block')->setBlockId($staticBlock)->toHtml() :
                Mage::getStoreConfig('grandriver_promomessages/config/freeshipping_message');
        }
        return $msg;
    }

    /**
     * Returns Free Shipping Shipping Method if enabled
     *
     * @return null|Mage_Shipping_Carrier_Freeshipping
     */
    public function getFreeShippingMethod() {
        $carrier = null;
        foreach (Mage::getSingleton('shipping/config')->getActiveCarriers() as $carrierCode => $carrierModel) {
            if ($carrierCode != 'freeshipping') { continue; }
            $carrier = $carrierModel;
            break;
        }
        return $carrier;
    }

    /**
     * Returns a collection of all sales rules with full order free shipping set
     *
     * @return mixed
     */
    public function getFreeShippingPromotions() {
    	$freeShippingPromotions = Mage::getModel('salesrule/rule')
            ->getCollection()
            ->addFieldToFilter('simple_free_shipping', array('eq'=>2))
            ->addFieldToFilter('is_active', array('eq'=>1))
			->addFieldToFilter('coupon_type', array('eq'=>1))
			->addFieldToFilter('from_date', array(
					 array('lteq'=>now())
					,array('null'=>true)
					))
			->addFieldToFilter('to_date', array(
					 array('gteq'=>now())
					,array('null'=>true)
			))
			->addWebsiteFilter(Mage::app()->getWebsite()->getId());
            
            return $freeShippingPromotions;
    }

    /**
     * Returns condition set array for lowest subtotal free shipping promotion
     *
     * @return null|array
     */
    public function getFreeShippingPromotionCondition() {
        $result = null;
        $freeShippingPromotions = $this->getFreeShippingPromotions();
        $value = 0;
        foreach ($freeShippingPromotions as $rule) {
            if ($rule && is_array($this->getRuleConditions($rule))) {
                foreach ($this->getRuleConditions($rule) as $condition) {
                    if (!in_array('base_subtotal', $condition)) { continue; }
                    if (isset($condition['value']) && $condition['value']) {
                        $newValue = $condition['value'];
                        if (!$value || $value && ($newValue <= $value)) {
                            $value = $newValue;
                            $result = $condition;
                        }
                    }
                }
            }
        }
        return $result;
    }

    /**
     * Getter for free shipping achieved flag
     *
     * @return bool
     */
    public function getFreeShippingAchieved() {
        return $this->_isFreeShipping;
    }

    /**
     * Setter for free shipping achieved flag
     *
     * @return bool
     */
    public function setFreeShippingAchieved($bool) {
        $this->_isFreeShipping = $bool;
    }

    /**
     * Return unserialized array of conditions for a given sales rule
     *
     * @param $rule
     * @return array
     */
    public function getRuleConditions($rule) {
        $conditions = unserialize($rule->getConditionsSerialized());
        if (isset($conditions['conditions']))
        {
            return $conditions['conditions'];
        }
    }

    /**
     * Return unserialized array of actions for a given sales rule
     *
     * @param $rule
     * @return array
     */
    public function getRuleActions($rule) {
        $conditions = unserialize($rule->getActionsSerialized());
        return $conditions['actions'];
    }

    /**
     * Calculates amount left to achieved free shipping based on a sales rule
     *
     * @return float|int
     */
    public function calcFreeShippingPromotion() {
        $result = 0;
        if ($condition = $this->getFreeShippingPromotionCondition()) {
            $subtotal = $this->getSubtotal();
            $operator = $condition['operator'];
            $value = $condition['value'];
            $amountLeft = (float) $value - (float) $subtotal;
            $isFreeShipping = $this->getFreeShippingAchieved();
            if (!$isFreeShipping) {
                switch ($operator) {
                    case '>=' :
                        $isFreeShipping = $amountLeft <= 0;
                        break;
                    case '>' :
                        $isFreeShipping = $amountLeft < 0;
                        break;
                    case '<=' :
                        $isFreeShipping = $amountLeft >= 0;
                        break;
                    case '<' :
                        $isFreeShipping = $amountLeft > 0;
                        break;
                    case '==' :
                        $isFreeShipping = $amountLeft == 0;
                        break;
                }
                $this->setFreeShippingAchieved($isFreeShipping);
            }
            $result = $this->getFreeShippingAchieved() ? 0 :  $amountLeft;
        }
        return $result;
    }

    /**
     * Calculates amount left to achieve free shipping from free shipping method
     *
     * @return float|int
     */
    public function calcFreeShippingMethod() {
        $result = 0;
        $useDiscounts = Mage::helper('grandriver_promomessages')->getUseSubtotalWithDiscount();
        if ($carrierModel = $this->getFreeShippingMethod()) {
            if ($freeShippingSubtotal = $carrierModel->getConfigData('free_shipping_subtotal')) {
                $subTotal = $useDiscounts ? $this->getSubTotalWithDiscount() : $this->getSubTotal();
                $amountLeft = (float) $freeShippingSubtotal - (float) $subTotal;
                $this->setFreeShippingAchieved($amountLeft <= 0);
                $result = $this->getFreeShippingAchieved() ? 0 : $amountLeft;
            }
        }
        return $result;
    }

    /**
     * Returns cart's subtotal
     * @return float
     */
    public function getSubTotal() {
        return $this->getQuote()->getSubtotal();
    }

    public function getSubTotalWithDiscount() {
        return $this->getQuote()->getSubtotalWithDiscount();
    }

}