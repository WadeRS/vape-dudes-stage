<?php
class GrandRiver_PromoMessages_Block_Lightbox extends Mage_Core_Block_Template {

    protected $_couponCode;
    protected $_errorMsg;
    protected $_varName;
    protected $_msgFormat;

    public function __construct() {
        $this->_msgFormat = Mage::getStoreConfig('grandriver_promomessages/config/coupon_code_lightbox_message',Mage::app()->getStore());
        $this->_varName = Mage::getStoreConfig('grandriver_promomessages/config/coupon_code_get_var',Mage::app()->getStore());
    }

    public function getCouponCode() {
        if (!$this->_couponCode) {
            $this->_couponCode = $this->getCouponCodeInUrl();
        }
        return $this->_couponCode;
    }

    public function getErrorMsg() {
        return $this->_errorMsg;
    }

    public function getCouponCodeInUrl() {
        $param = $this->getVarName();
        return Mage::app()->getFrontController()->getRequest()->getParam($param);
    }

    /**
     * loads coupon model by coupon code and returns it
     * @param $couponCode
     * @return mixed
     */
    public function getCouponByCode($couponCode) {
        return Mage::getModel('salesrule/coupon')->loadByCode($couponCode);
    }

    public function validateCoupon($couponCode) {
        $isValid = false;
        if (strlen($couponCode)) {

            $exCoupon = $this->getCouponByCode($couponCode);
            // check to see if coupon exists
            
            if ($exCoupon->getId()) {
                // check to see if it's expired
                if ($exCoupon->getExpirationDate() && strtotime($exCoupon->getExpirationDate()) < Mage::getModel('core/date')->timestamp(time())) {
                    $this->_errorMsg = $this->__('Coupon code "%s" has expired. Please check your email for new offers coming soon!', Mage::helper('core')->htmlEscape($couponCode));
                }
                // check to see if it has reached it's usage limit
                if ($exCoupon->getUsageLimit() && $exCoupon->getTimesUsed() >= $exCoupon->getUsageLimit()) {
                    $this->_errorMsg =  $this->__('Coupon code "%s" has reached it\'s usage limit.', Mage::helper('core')->htmlEscape($couponCode));
                }
                // check to see the customer is in an allowed group
                else if (!in_array($this->getCustomerGroupId(), $this->getRuleCustomerGroupsIds($exCoupon->getRuleId()))) {
                    $msg = 'Coupon code "%s" is not active for your customer group.';
                    $msg .= ($this->getCustomerGroupId() == 0) ? '<br />If you have an active account please <a href="' . Mage::helper("customer")->getRegisterUrl() . '">login</a> and try again.': '';
                    $this->_errorMsg = $this->__($msg, Mage::helper('core')->htmlEscape($couponCode));
                }
                else {
                    $isValid = true;
                }
            }
            // coupon doesn't exist
            else {
                $this->_errorMsg = $this->__('Coupon code "%s" does not exist. Sign up to receive our newsletter and get our latest specials!', Mage::helper('core')->htmlEscape($couponCode));
            }

        }
        return $isValid;
    }

    /**
     * Get checkout session model instance
     *
     * @return Mage_Checkout_Model_Session
     */
    protected function _getSession()
    {
        return Mage::getSingleton('checkout/session');
    }

    /**
     * gets current customer's customer group id.
     * @return int
     */
    public function getCustomerGroupId() {
        return Mage::getSingleton('customer/session')->getCustomerGroupId();
    }

    /**
     * gets array of salesrule assigned customer ids
     * @param $salesRuleId
     * @return mixed
     */
    public function getRuleCustomerGroupsIds($salesRuleId) {
        return Mage::getModel('salesrule/rule')->load($salesRuleId)->getCustomerGroupIds();
    }

    public function getMessageFormat() {
        return $this->_msgFormat;
    }

    public function getVarName() {
        return $this->_varName;
    }

    public function getRuleLabel($couponCode) {
        $coupon = $this->getCouponByCode($couponCode);
        $rule = Mage::getModel('salesrule/rule')->load($coupon->getRuleId());
        return $rule->getStoreLabel(Mage::app()->getStore());
    }

    public function getMessage($couponCode) {
        return sprintf($this->getMessageFormat(), $couponCode);
    }

}