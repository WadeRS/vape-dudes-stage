<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Core
 * @copyright   Copyright (c) 2011 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


/**
 *
 * @category   Bluejalappeno
 * @package    Bluejalappeno_Orderexport
 * @copyright  Copyright (c) 2012 Wimbolt Ltd (http://www.bluejalappeno.com)
 * @license    http://www.bluejalappeno.com/license.txt - Commercial license
 */

class Bluejalappeno_Orderexport_Model_Export_Vatexempt extends Bluejalappeno_Orderexport_Model_Export_Abstractcsv
{

	public function exportOrders($orders)
	{
		$csv = '';
		$purchases = array();

		$csvHeader = array('"'.Mage::helper('adminhtml')->__('Date of Purchase').'"','"'.Mage::helper('adminhtml')->__('Order Number').'"', '"'.Mage::helper('adminhtml')->__('Customer Name').'"', '"'.Mage::helper('adminhtml')->__('Company Name').'"','"'.Mage::helper('adminhtml')->__('VAT Number').'"','"'.Mage::helper('adminhtml')->__('Order Total').'"','"'.Mage::helper('adminhtml')->__('Product Purchased').'"');
		$csv .= implode(',', $csvHeader)."\n";

		foreach ($orders as $purchase){
			$purchase = Mage::getModel('sales/order')->loadByAttribute('entity_id',$purchase);

			if($purchase->getCustomerGroupId() == Mage::getStoreConfig('order_export/export_orders/group'))
			{
				$purchases[] = $purchase;
			}
			else{
				continue;
			}
		}

		foreach ($purchases as $order){

			$customerDetails = $order->getBillingAddress();
			$orderdate = substr_replace($order->getData('created_at'), '', -8);
			$customerFirstName = $customerDetails->getFirstname();
			$customerLastName = $customerDetails->getLastname();
			$companyName = $customerDetails->getCompany();
			$fullName = $customerFirstName.''.''.$customerLastName;
			$vatNumber = $order->getData('customer_taxvat');
			$orderId = $order->getData('increment_id');
			$grandTotal = $order->getData('base_grand_total');
			$productsPurchased = $this->getItemsOrdered($order->getItemsCollection());

			$csvData = array($orderdate,$orderId,$fullName,$companyName,$vatNumber,$grandTotal,$productsPurchased);

			foreach ($csvData as $cell) {
				$cell = '"'.str_replace('"', '""', $cell).'"';
			}
			$csv .= implode(',', $csvData)."\n";
			Mage::helper('orderexport')->setExported($order->getId());

		}

		$fileName = 'order_export_VATexempt'.date("Ymd_His").'.csv';
		header('Pragma: public');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');

		header("Content-type: application/octet-stream");
		header("Content-disposition: attachment; filename=".$fileName);
		echo $csv;
		exit;
	}


 protected function getItemsOrdered($orderItems)
 {
 	$products = array();

     	foreach($orderItems as $item){

     		$products[] = $item->getName();

     	}

     	$productsOrdered = implode(",",$products);
     	return $productsOrdered;
 }


}