<?php
require_once 'abstract.php';
 
class Mage_Shell_Pricerules extends Mage_Shell_Abstract
{
 
    public function run()
    {
        ini_set('memory_limit', '1024M');
        $this->db = Mage::getSingleton('core/resource')->getConnection('core_read');
 
        if (isset($this->_args['refresh'])) {
          echo "Refreshing catalog price rules\n";
 
          if ($this->_args['category']) {
            $ids = array();
            $category_entity_id = $this->db->fetchOne("select a.entity_id from catalog_category_entity as a inner join catalog_category_entity_varchar as b on a.entity_id = b.entity_id where b.attribute_id = (select attribute_id from eav_attribute where attribute_code = 'name' and entity_type_id = (select entity_type_id from eav_entity_type where entity_type_code = 'catalog_category')) and parent_id = " . Mage::app()->getWebsite(true)->getDefaultStore()->getRootCategoryId() . " AND b.value = '" . addslashes($this->_args['category']) . "';");
            $_category = new Mage_Catalog_Model_Category();
            $_category->load($category_entity_id);
 
            $subs = $_category->getAllChildren(true);
            foreach($subs as $sub_id) {
              $_subcategory = new Mage_Catalog_Model_Category();
              $_subcategory->load($sub_id);
              $ids = array_merge($ids, $_subcategory->getProductCollection()->getAllIds());
            }
 
            $collection = Mage::getResourceModel('catalog/product_collection')
                ->addAttributeToSelect('sku')
                ->addFieldToFilter('entity_id', $ids);
          }
          else if ($attribute = array_pop(array_keys($this->_args))) {
            $attributeModel = Mage::getModel('eav/config')->getAttribute('catalog_product', $attribute)->getSource();
 
            if (!$attributeModel->getAttribute()->getId())
              throw new Exception("The attribute '$attribute' does not exist");                
 
            $attributeId = $attributeModel->getOptionId($this->_args[$attribute]);
            if (!$attributeId)
              throw new Exception("The attribute value '" . $this->_args[$attribute] . "' does not exist for attribute '$attribute'");                 
 
            $collection = Mage::getModel('catalog/product')
              ->getCollection()
              ->addAttributeToSelect('sku')
              ->addAttributeToFilter($attribute,$attributeId)
              ->addAttributeToFilter('type_id', 'configurable');
          }
          else {
            $collection = Mage::getModel('catalog/product')
              ->getCollection()
              ->addAttributeToSelect('sku')
              ->addAttributeToFilter('type_id', 'configurable');          
          }
 
          $count = count($collection);
          $i = 0;
          
          foreach($collection as $product) {
            $i++;
            $productWebsiteIds = $product->getWebsiteIds();
 
            $rules = Mage::getModel('catalogrule/rule')->getCollection()
                ->addFieldToFilter('is_active', 1);
 
            foreach ($rules as $rule) {
                $websiteIds = array_intersect($productWebsiteIds, $rule->getWebsiteIds());
                $rule->applyToProduct($product, $websiteIds);
                $rule->clearInstance();
            }
 
            echo "Applied rules to " . $product->getSku() . " (" . number_format(memory_get_usage() / 1024 / 1024 / 1024, 2) . "G, " . ($count - $i) . " products left)\n";
 
            $product->clearInstance();
          }
 
          $resource = Mage::getResourceSingleton('catalogrule/rule');
          $resource->applyAllRulesForDateRange();
 
          echo "Reindexing catalog_product_price\n";
          exec('php indexer.php --reindex catalog_product_price');
 
          echo "Finished\n";
        } else {
            echo $this->usageHelp();
        }
    }
 
    public function usageHelp()
    {
        return <<<USAGE
Usage:  php -f pricerules.php -- [options]
 
  This CLI utility refreshes the catalog price rules, you can either specify a category:
  
  --refresh --category "Jackets"
 
  or specify an attribute and value:
 
  --refresh --attribute "value"
 
  
USAGE;
    }
}
 
require_once str_replace('shell','',getcwd()) . 'app/Mage.php';
 
$shell = new Mage_Shell_Pricerules();
$shell->run();