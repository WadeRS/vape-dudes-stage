<?php
/* DO NOT MODIFY THIS FILE! THIS IS TEMPORARY FILE AND WILL BE RE-GENERATED AS SOON AS CACHE CLEARED. */

/**
 * Shopping Cart Editor
 *
 * @category:    Aitoc
 * @package:     Aitoc_Aiteditablecart
 * @version      2.1.8
 * @license:     QJLpuB2aEkjYR94VLCHRHtZ5qCz3k9pHhguxuAjiFY
 * @copyright:   Copyright (c) 2014 AITOC, Inc. (http://www.aitoc.com)
 */
/**
 * @copyright  Copyright (c) 2009 AITOC, Inc. 
 */

class Aitoc_Aiteditablecart_Model_Rewrite_FrontBundleProductType extends Mage_Bundle_Model_Product_Type
{
    // override parent    
    
    public function getOptionsCollection($product = null)
    {
// start aitoc code

if (!$this->getStoreFilter($product))
{
    $this->setStoreFilter($product->getStoreId(), $product); // ait
}

// finish aitoc code
        
        return parent::getOptionsCollection($product);
    }    
}



class Wizkunde_ConfigurableBundle_Model_Bundle_Product_Type extends Aitoc_Aiteditablecart_Model_Rewrite_FrontBundleProductType
{
    /**
     * @var Varien_Object buyRequest
     */
    protected $_buyRequest = null;

    /**
     * Retrive bundle selections collection based on used options
     *
     * @param array $optionIds
     * @param Mage_Catalog_Model_Product $product
     * @return Mage_Bundle_Model_Mysql4_Selection_Collection
     */
    public function getSelectionsCollection($optionIds, $product = null)
    {
        if (!$this->getProduct($product)->hasData($this->_keySelectionsCollection)) {
            $selectionsCollection = Mage::getResourceModel('bundle/selection_collection')
                ->addAttributeToSelect(Mage::getSingleton('catalog/config')->getProductAttributes())
                ->setFlag('require_stock_items', true)
                ->setPositionOrder()
                ->addStoreFilter($this->getStoreFilter($product))
                ->setOptionIdsFilter($optionIds);

            $this->getProduct($product)->setData($this->_keySelectionsCollection, $selectionsCollection);
        }

        return $this->getProduct($product)->getData($this->_keySelectionsCollection);
    }

    /**
     * Get Configurable Options in JSON format
     *
     * @param $_product
     * @return string
     */
    public function getOptionsJSON($_product)
    {
        $optionsCollection = $this->getOptionsCollection($_product);

         $json = array();

         foreach($this->getSelectionsCollection($this->getOptionsIds($_product)) as $selection)
         {
             foreach($optionsCollection as $option) {
                 if($option->getId() == $selection->getOptionId()) {
                     $currentOption = $option;
                 }
             }

             if($selection->isConfigurable()) {
                 $configurableProduct = new Mage_Catalog_Block_Product_View_Type_Configurable();
                 $configurableProduct->setData('product', $selection);

                 // Have to decode to prevent double json encoding
                 // Collect options applicable to the configurable product
                 $productAttributeOptions = $selection->getTypeInstance(true)->getConfigurableAttributesAsArray($selection);
                 $attributeOptions = array();
                 foreach ($productAttributeOptions as $productAttribute) {
                     foreach ($productAttribute['values'] as $attribute) {
                         $attributeOptions[$productAttribute['label']][$attribute['value_index']] = $attribute['store_label'];
                     }
                 }

                 $json[$selection->getOptionId()]['option']['required'] = (bool)$currentOption->getRequired();
                 $json[$selection->getOptionId()]['attributes'] = $attributeOptions;
                 $json[$selection->getOptionId()]['product'] = Zend_Json::decode($configurableProduct->getJsonConfig());
             } else {
                 $json[$selection->getOptionId()]['option']['required'] = (bool)$currentOption->getRequired();
                 $json[$selection->getOptionId()]['product'] = $selection->getData();
             }
         }

         return Zend_Json::encode($json);
    }

    /**
     * Retrieve bundle selections collection based on ids
     *
     * @param array $selectionIds
     * @param Mage_Catalog_Model_Product $product
     * @return Mage_Bundle_Model_Mysql4_Selection_Collection
     */
    public function getSelectionsByIds($selectionIds, $product = null)
    {
        sort($selectionIds);

        $storeId = $this->getProduct($product)->getStoreId();
        $usedSelections = Mage::getResourceModel('bundle/selection_collection')
            ->addAttributeToSelect('*')
            ->setFlag('require_stock_items', true)
            ->addStoreFilter($this->getStoreFilter($product))
            ->setStoreId($storeId)
            ->setPositionOrder()
            ->setSelectionIdsFilter($selectionIds)
        ;

        if (!Mage::helper('catalog')->isPriceGlobal() && $storeId) {
            $websiteId = Mage::app()->getStore($storeId)->getWebsiteId();
            $usedSelections->joinPrices($websiteId);
        }

        $this->getProduct($product)->setData($this->_keyUsedSelections, $usedSelections);
        $this->getProduct($product)->setData($this->_keyUsedSelectionsIds, $selectionIds);

        return $usedSelections;
    }
}

