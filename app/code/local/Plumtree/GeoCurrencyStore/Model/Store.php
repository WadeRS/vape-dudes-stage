<?php
class Plumtree_GeoCurrencyStore_Model_Store extends Mage_Core_Model_Store
{
     public function getDefaultCurrencyCode()
    {
         $result = $this->getConfig(Mage_Directory_Model_Currency::XML_PATH_CURRENCY_DEFAULT);
  
  	  	$url = 'http://www.geoplugin.net/php.gp?ip='.$_SERVER['REMOTE_ADDR'];		
		$curl = curl_init($url);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
 		$data = curl_exec($curl);
		curl_close($curl);
	 	
		$myarray = unserialize($data);
 	 	$currencyCode = $myarray['geoplugin_currencyCode'];
		$allowedCurrencies = Mage::getModel('directory/currency')->getConfigAllowCurrencies();		
	 	
		if(!in_array($currencyCode, $allowedCurrencies)) {
			return $result;
		}
		else
		{
			return $currencyCode;
		}
     }
}
?>