<?php
/**
 * Magento Webshopapps Order Export Module
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category   Webshopapps
 * @package    Webshopapps_OrderExport
 * @copyright  Copyright (c) 2010 Zowta Ltd (http://www.webshopapps.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     Genevieve Eddison <sales@webshopapps.com>
 * */
class Bluejalappeno_Orderexport_Model_Export_Rmdmo extends Bluejalappeno_Orderexport_Model_Export_Abstractcsv
{
    const ENCLOSURE = '"';
    const DELIMITER = ',';

    /**
     * Concrete implementation of abstract method to export given orders to csv file in var/export.
     *
     * @param $orders List of orders of type Mage_Sales_Model_Order or order ids to export.
     * @return String The name of the written csv file in var/export
     */
    public function exportOrders($orders) 
    {
        $fileName = 'order_export_'.date("Ymd_His").'.csv';
        $fp = fopen(Mage::getBaseDir('export').'/'.$fileName, 'w');
        
        $this->writeHeadRow($fp);
        foreach ($orders as $orderId) {
        	$order = Mage::getModel('sales/order')->load($orderId);
        	if (!$order->getIsVirtual()) {
            	$this->writeOrder($order, $fp);
            	Mage::helper('orderexport')->setExported($orderId);
        	}
        }
        fclose($fp);
        
        return $fileName;
    }

    /**
	 * Writes the head row with the column names in the csv file.
	 * 
	 * @param $fp The file handle of the csv file
	 */
    protected function writeHeadRow($fp) 
    {
        $this->fputcsv($fp, $this->getHeadRowValues(), self::DELIMITER, self::ENCLOSURE);
    }

    /**
	 * Writes the row(s) for the given order in the csv file.
	 * A row is added to the csv file for each ordered item. 
	 * 
	 * @param Mage_Sales_Model_Order $order The order to write csv of
	 * @param $fp The file handle of the csv file
	 */
    protected function writeOrder($order, $fp) 
    {
        $record = $this->getCommonOrderValues($order);
     	$this->fputcsv($fp, $record, self::DELIMITER, self::ENCLOSURE);
	   
    }

    /**
	 * Returns the head column names.
	 * 
	 * @return Array The array containing all column names
	 */
    protected function getHeadRowValues() 
    {
       $headings =  array(
            'Service Reference',
            'Service',
       		'Service Enhancement',
       		'Service Class',
            'Recipient',
            'Recipient Address line 1',
            'Recipient Address line 2',
       		'Recipent Postcode',
            'Recipient Post Town',
            'Recipient Country Code',
       		'Recipient Contact Name',
            'Recipient Email Address',
       		'Recipient Tel #',
            'Reference #',
            'Items',
            'Weight(kgs)')
       		;
        	return $headings;
    	
    }

    /**
	 * Returns the values which are identical for each row of the given order. These are
	 * all the values which are not item specific: order data, shipping address, billing
	 * address and order totals.
	 * 
	 * @param Mage_Sales_Model_Order $order The order to get values from
	 * @return Array The array containing the non item specific values
	 */
    protected function getCommonOrderValues($order) 
    {
        $shippingAddress = $order->getShippingAddress() ? $order->getShippingAddress() : $order->getBillingAddress();
        $billingAddress = $order->getBillingAddress();
        return array(
	         	'Service Reference 1', //?
	         	$order->getShippingDescription(),//'Service', ?
	         	'', //Service Enhancement e.g. 'EMAIL' or 'SMS' or "Email & SMS'
	         	'', //'Service Class' ?
	         	$shippingAddress->getName(),
	         	$shippingAddress->getStreet1(),
	         	$shippingAddress->getStreet2()? $shippingAddress->getStreet2() : '',
	         	$shippingAddress->getPostcode(),
	         	$shippingAddress->getCity(),
	         	$shippingAddress->getCountry(),
	         	$shippingAddress->getData("company") ? $shippingAddress->getData("company") : '',
	         	$order->getCustomerEmail(),
	         	$shippingAddress->getData("telephone") ? $shippingAddress->getData("telephone") : '',
	         	$order->getRealOrderId(),
	         	$order->getTotalItemCount(),
	         	strval(number_format((float)$order->getWeight(), 3)) 
	     );
    }

}
?>