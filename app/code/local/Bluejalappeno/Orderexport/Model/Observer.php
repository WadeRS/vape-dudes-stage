<?php
/**
 *
 * @category   Bluejalappeno
 * @package    Bluejalappeno_Orderexport
 * @copyright  Copyright (c) 2012 Wimbolt Ltd (http://www.bluejalappeno.com)
 * @license    http://www.bluejalappeno.com/license.txt - Commercial license
 */


class Bluejalappeno_Orderexport_Model_Observer
{
	protected $_debug;
	public function exportOrders()
	{
		if (!Mage::helper('bjalcommon')->isModuleEnabled('Bluejalappeno_Orderexport', 'order_export/export_orders/active')) { return;}
		if (Mage::getStoreConfig('order_export/export_orders/debug')) $this->_debug = true;

		$collection = Mage::getResourceModel('sales/order_collection');
		$orders = array();
		foreach ($collection as $order) {
			if (!Mage::helper('orderexport')->isExported($order->getId())) {
				array_push($orders,$order->getId());
			}
		}
		if(count($orders) > 0) {
			$file ='';
			$exportedResult = 0;
			switch(Mage::getStoreConfig('order_export/export_orders/output_type')){
				case 'Standard':
					$file = Mage::getModel('orderexport/export_csv')->exportOrders($orders);
					break;
				case 'Sage':
					$file = Mage::getModel('orderexport/export_sage')->exportOrders($orders);
					break;
				case 'VatExempt' :
					$file = Mage::getModel('orderexport/export_vatexempt')->exportOrders($orders);
					break;
				case 'RMDMO' :
					$file = Mage::getModel('orderexport/export_rmdmo')->exportOrders($orders);
					break;
				case 'Highrise':
					$failedList = '';
					$successCount = 0;
					$failCount = 0;
					try {
						$results = Mage::getModel('orderexport/export_highrise')->exportOrders($orders);
						foreach ($results as $orderid => $status) {
							if ($status > 0 ) $successCount++;
							else {
								$failedList.= $orderid .' ';
								$failCount++;
							}
						}
						if ($failedCount > 0 ) {
							$exportedResult = 	$failedList;
						}
					}
					catch (Exception $e) {
						Mage::log($e->getMessage());
						Mage::getSingleton('adminhtml/session')->addError(Mage::helper('sales')->__($e->getMessage()));
					}

					if ($failCount > 0) {
						$failedString = $successCount .' order(s) have been imported. The following orders failed to import: ' .$failedList;
						Mage::helper('wsalogger/log')->postInfo('WebShopApps Order Export','Export via Highrise','There was an issue exporting to Highrise: ' .$failedString);

					}
					else {
						if ($this->_debug) {
							Mage::helper('wsalogger/log')->postDebug('WebShopApps Order Export','Export to Highrise','Successfully exported orders ');
						}
					}
					break;
			}

			if ($file != '') {
				$ftpResult = $this->_ftpFile($file);
				if ($ftpResult == 0) {
					if ($this->_debug) {
						Mage::helper('wsalogger/log')->postDebug('WebShopApps Order Export','Export via FTP','Successfully exported orders ');
					}
				}
				else if ($ftpResult == -1) {
					if ($this->_debug) {
						Mage::helper('wsalogger/log')->postDebug('WebShopApps Order Export','Export via FTP','No FTP credentials supplied in configuration');
					}
				}
				else {
					Mage::helper('wsalogger/log')->postInfo('WebShopApps Order Export','Export via FTP and there was a problem','FTP error number ' .$ftpResult);
				}
				$emailResult = $this->_emailFile($file);
				if ($emailResult == 0) {
					if ($this->_debug) {
						Mage::helper('wsalogger/log')->postDebug('WebShopApps Order Export','Export via email','Successfully exported orders ');
					}
				}
				else if ($emailResult == -1) {
					if ($this->_debug) {
						Mage::helper('wsalogger/log')->postDebug('WebShopApps Order Export','Export via email','No email credentials supplied in configuration');
					}
				}
				else {
					Mage::helper('wsalogger/log')->postInfo('WebShopApps Order Export','Export via email and there was a problem','Email error number ' .$emailResult);
				}
			}
		}

	}

	public function exportSummary()
	{
		if (!Mage::helper('bjalcommon')->isModuleEnabled('Bluejalappeno_Orderexport', 'order_export/export_orders/active')) {
			return;
		}
		if (Mage::getStoreConfig('order_export/export_orders/debug')) $this->_debug = true;

		switch (Mage::getStoreConfig('order_export/automatic/frequency_extra')){
			case Bluejalappeno_Orderexport_Model_Config_Backend_Export_Summaryfrequency::CRON_UNUSED :
				return;
			case Bluejalappeno_Orderexport_Model_Config_Backend_Export_Summaryfrequency::CRON_DAILY :
				$searchDate =  date('Y-m-d', strtotime("-1 day"));
				break;
			case Bluejalappeno_Orderexport_Model_Config_Backend_Export_Summaryfrequency::CRON_WEEKLY :
				$searchDate = date('Y-m-d', strtotime("-1 week"));
				break;
			case Bluejalappeno_Orderexport_Model_Config_Backend_Export_Summaryfrequency::CRON_MONTHLY :
				$searchDate = date('Y-m-d', strtotime("-1 month"));
				break;
		}

		$collection = Mage::getModel('sales/order')->getCollection()
		->addAttributeToFilter('created_at', array('from'  => $searchDate));
		$orders = array();
		foreach ($collection as $order) {

			if (Mage::helper('orderexport')->isExported($order->getId())) {
				array_push($orders,$order->getId());
			}
		}

		if(count($orders) > 0) {
			$file ='';
			$exportedResult = 0;
			switch(Mage::getStoreConfig('order_export/export_orders/output_type')){
				case 'Standard':
					$file = Mage::getModel('orderexport/export_csv')->exportOrders($orders);
					break;
				case 'Sage':
					$file = Mage::getModel('orderexport/export_sage')->exportOrders($orders);
					break;
				case 'VatExempt' :
					$file = Mage::getModel('orderexport/export_vatexempt')->exportOrders($orders);
					break;
				case 'RMDMO' :
					$file = Mage::getModel('orderexport/export_rmdmo')->exportOrders($orders);
					break;
				case 'Highrise':
					$failedList = '';
					$successCount = 0;
					$failCount = 0;
					try {
						$results = Mage::getModel('orderexport/export_highrise')->exportOrders($orders);
						foreach ($results as $orderid => $status) {
							if ($status > 0 ) $successCount++;
							else {
								$failedList.= $orderid .' ';
								$failCount++;
							}
						}
						if ($failedCount > 0 ) {
							$exportedResult = 	$failedList;
						}
					}
					catch (Exception $e) {
						Mage::log($e->getMessage());
						Mage::getSingleton('adminhtml/session')->addError(Mage::helper('sales')->__($e->getMessage()));
					}

					if ($failCount > 0) {
						$failedString = $successCount .' order(s) have been imported. The following orders failed to import: ' .$failedList;
						Mage::helper('wsalogger/log')->postInfo('WebShopApps Order Export','Export via Highrise','There was an issue exporting to Highrise: ' .$failedString);

					}
					else {
						if ($this->_debug) {
							Mage::helper('wsalogger/log')->postDebug('WebShopApps Order Export','Export to Highrise','Successfully exported orders ');
						}
					}
					break;
			}

			if ($file != '') {
				$emailResult = $this->_emailFile($file);
				if ($emailResult == 0) {
					if ($this->_debug) {
						Mage::helper('wsalogger/log')->postDebug('WebShopApps Order Export','Export via email','Successfully exported orders ');
					}
				}
				else if ($emailResult == -1) {
					if ($this->_debug) {
						Mage::helper('wsalogger/log')->postDebug('WebShopApps Order Export','Export via email','No email credentials supplied in configuration');
					}
				}
				else {
					Mage::helper('wsalogger/log')->postInfo('WebShopApps Order Export','Export via email and there was a problem','Email error number ' .$emailResult);
				}
			}
		}

	}

	protected function _ftpFile($fileName)
	{
		$dir         = Mage::getBaseDir('export').'/';
		$ftpServer   = Mage::getStoreConfig('order_export/automatic/ftp_location');
		$ftpUserName = Mage::getStoreConfig('order_export/automatic/ftp_username');
		$ftpPass     = Mage::getStoreConfig('order_export/automatic/ftp_password');
		$ftpPath     = trim(Mage::getStoreConfig('order_export/automatic/ftp_path'), '/');
		if ($ftpServer != '' && $ftpPass != '' && $ftpPath != '') {
			if ($ftpPath) {
				$ftpPath = $ftpPath.'/';
			}
			try {
				$url = 'ftp://'.$ftpUserName.':'.$ftpPass.'@'.$ftpServer.'/'.$ftpPath.$fileName;
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_UPLOAD, 1);
				curl_setopt($ch, CURLOPT_INFILE, fopen($dir.$fileName, 'r'));
				curl_setopt($ch, CURLOPT_INFILESIZE, filesize($dir.$fileName));
				curl_exec($ch);
				$error_no = curl_errno($ch);
				curl_close($ch);
				if ($error_no == 0) {
					Mage::helper('wsalogger/log')->postDebug('WebShopApps Order Export','FTP Status',' FTP was successful ');
				} else {
					Mage::helper('wsalogger/log')->postWarning('WebShopApps Order Export','FTP Generated Error', ' Problem uploading to URL ' .$url .' : error was ' .$error_no);
				}

			} catch (Exception $e) {
				Mage::helper('wsalogger/log')->postWarning('WebShopApps Order Export','FTP Exception',$e->getMessage() .' from URL ' .$url);
			}
			return $error_no;
		}
		return -1;
	}

	protected function _emailFile($fileName)
	{
		if (Mage::getStoreConfig('order_export/automatic/email') != '') {
			$errorNo = 0;
			if (Mage::getStoreConfigFlag('system/smtp/disable')) {

				return $this;
			}
			$file = file_get_contents(Mage::getBaseDir('export').'/'.$fileName);

			$mail = new Zend_Mail();
			$mail->setBodyText('See attached export of orders');
			$mail->setFrom(Mage::getStoreConfig('contacts/email/recipient_email'))
			->addTo(Mage::getStoreConfig('order_export/automatic/email'), '')
			->setSubject('Order Export');

			$attachment = $mail->createAttachment($file);
			$attachment->filename = $fileName;

			try {
				$mail->send();
				Mage::helper('wsalogger/log')->postDebug('WebShopApps Order Export','Email Status',' email was successful ');
			}
			catch (Exception $e) {
				$errorNo = 99;
				Mage::helper('wsalogger/log')->postWarning('WebShopApps Order Export','Send email exception',$e->getMessage() .' from file name ' .$fileName);
			}
			return $errorNo;
		}
		return -1;
	}

	public function addMassAction($observer) {
		$block = $observer->getEvent()->getBlock();
		if(($block instanceof Mage_Adminhtml_Block_Widget_Grid_Massaction || $block instanceof Enterprise_SalesArchive_Block_Adminhtml_Sales_Order_Grid_Massaction)
		&& strstr( $block->getRequest()->getControllerName(), 'sales_order') && Mage::helper('bjalcommon')->isModuleEnabled('Bluejalappeno_Orderexport', 'order_export/export_orders/active'))
		{
			$block->addItem('orderexport', array(
                'label' => Mage::helper('sales')->__('Export Orders'),
                'url' =>  Mage::getModel('adminhtml/url')->getUrl('*/sales_order_export/csvexport'),
			));
			if (Mage::getStoreConfig('order_export/export_orders/debug'))  {
				$block->addItem('orderexportcron', array(
	                'label' => Mage::helper('sales')->__('Export orders VIA CRON'),
	                'url' => Mage::getModel('adminhtml/url')->getUrl('*/sales_order_export/observerexport'),
				));

				$block->addItem('orderexportcronsumm', array(
	                'label' => Mage::helper('sales')->__('Export orders VIA CRON EXTRA'),
	                'url' => Mage::getModel('adminhtml/url')->getUrl('*/sales_order_export/observersummexport'),
				));
			}
		}

	}

}