V1.4   -  Added support for additional attributes in standard csv export

v1.5   -  Resolved issue with attributes in config

v1.6 -    Resolved issue with Highrise export

v2.0 - 	  Only completed or closed orders are exported for Sage format

v2.2 - 	  Updated to remove conflict with other extensions using Sales Order Grid menu

v2.3 -	  Altered street field formating in address

v2.4 -    Added Select All option to Orders screen

v2.5 - 	  Updated export for Item attributes of type dropdown and Store ID

v2.6 - 	  Updated export of configurable products in condensed order export and put base currency switch in for Sage format

v2.7 - 	  Updated export of SKU when items have custom options

v2.8 - 	  Added base row total to itemised Standard CSV export

v2.9 -	  Added base currency switch to Standard CSV export and Delivery row to Sage export

v2.10 -   Added categories to itemised Standard CSV export

v2.11 -   Added filterable exported flag to orders grid

v2.12 -	  Removed UTF encoding for CSV output

v2.13 -	  Added cron with FTP and track orders export via Orders grid

v2.14 -   Blue Jalappeno release

v2.15 -	  Added additional cron frequencies

v2.16 -   Resolved issue with table prefixes

v2.17 -   Updated Sage grand total and Item totals in standard CSV

v2.18 -	  Added 'updated at' column to sales order grid and config option to choose encoding for csv file - removed rewrites

v2.19 -	  Added export of order attributes (configurable) and export of coupon code

v2.20 -	  Updated observer for Enterprise compatibility

v2.21 -   Added customer group and order currency to export

v2.22 -   Modified encoding for UTF-8, added VAT number and gift message to export, CRON export menu only available under debug

v2.23 -   Changed itemisation for fixed price bundles

v2.24 -	  Modified URLs for SSL support

v2.25 -	  Added automatic email facility and shipped date to full CSV export

v2.26 -	  Added support for Amasty Order Attributes

v2.27 -   Resolved unselect issue

v2.28-	  Added cron summary feature
