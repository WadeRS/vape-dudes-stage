<?php
/**
 * Override of couponPostAction for better failure messaging
 *
 * @category    GrandRiver
 * @package     GrandRiver_PromoMessages
 * @copyright   Copyright (c) 2012 Grand River, Inc. (http://www.thegrandriver.com)
 * @developer   Marshel Barbash marshel.barbash@thegrandriver.com
 */

include_once('Mage/Checkout/controllers/CartController.php');
class GrandRiver_PromoMessages_Checkout_CartController extends Mage_Checkout_CartController
{

    /**
     * Initialize coupon
     */
    public function couponPostAction()
    {
        if (!Mage::helper('grandriver_promomessages')->getIsEnabled()) {
            parent::couponPostAction();
        }
        else {
            $this->couponPostWithBetterErrors();
        }
    }

    /**
     * loads coupon model by coupon code and returns it
     * @param $couponCode
     * @return mixed
     */
    public function getCouponByCode($couponCode) {
        return Mage::getModel('salesrule/coupon')->loadByCode($couponCode);
    }

    /**
     * gets array of salesrule assigned customer ids
     * @param $salesRuleId
     * @return mixed
     */
    public function getRuleCustomerGroupsIds($salesRuleId) {
        return Mage::getModel('salesrule/rule')->load($salesRuleId)->getCustomerGroupIds();
    }

    /**
     * gets current customer's customer group id.
     * @return int
     */
    public function getCustomerGroupId() {
        return Mage::getSingleton('customer/session')->getCustomerGroupId();
    }

    public function couponPostWithBetterErrors() {
        $couponCode = (string) $this->getRequest()->getParam('coupon_code');

        /**
         * No reason continue with empty shopping cart
         */
        if (!$this->_getCart()->getQuote()->getItemsCount()) {
            $this->_goBack();
            return;
        }

        if ($this->getRequest()->getParam('remove') == 1) {
            $couponCode = '';
        }
        $oldCouponCode = $this->_getQuote()->getCouponCode();

        if (!strlen($couponCode) && !strlen($oldCouponCode)) {
            $this->_goBack();
            return;
        }

        try {

            $this->_getQuote()->setCouponCode(strlen($couponCode) ? $couponCode : '')
                ->collectTotals()
                ->save();

            if (strlen($couponCode)) {
                if ($couponCode == $this->_getQuote()->getCouponCode()) {
                    $this->_getSession()->addSuccess(
                        $this->__('Coupon code "%s" was applied.', Mage::helper('core')->htmlEscape($couponCode))
                    );
                }
                else {
                    $exCoupon = $this->getCouponByCode($couponCode);
                    // check to see if coupon exists
                    if ($exCoupon->getId()) {
                        // check to see if it's expired
                        if ($exCoupon->getExpirationDate() && strtotime($exCoupon->getExpirationDate()) < Mage::getModel('core/date')->timestamp(time())) {
                            $this->_getSession()->addError(
                                $this->__('Coupon code "%s" has expired.', Mage::helper('core')->htmlEscape($couponCode))
                            );
                        }
                        // check to see if it has reached it's usage limit
                        if ($exCoupon->getUsageLimit() && $exCoupon->getTimesUsed() >= $exCoupon->getUsageLimit()) {
                            $this->_getSession()->addError(
                                $this->__('Coupon code "%s" has reached it\'s usage limit.', Mage::helper('core')->htmlEscape($couponCode))
                            );
                        }
                        // check to see the customer is in an allowed group
                        else if (!in_array($this->getCustomerGroupId(), $this->getRuleCustomerGroupsIds($exCoupon->getRuleId()))) {
                            $msg = 'Coupon code "%s" is not active for your customer group.';
                            $msg .= ($this->getCustomerGroupId() == 0) ? '<br />If you have an active account please <a href="' . Mage::helper("customer")->getRegisterUrl() . '">login</a> and try again.': '';
                            $this->_getSession()->addError(
                                $this->__($msg, Mage::helper('core')->htmlEscape($couponCode))
                            );
                        }
                        // not valid for other reasons
                        else {
                            $this->_getSession()->addError(
                                $this->__('Coupon code "%s" is not valid.', Mage::helper('core')->htmlEscape($couponCode))
                            );
                        }
                    }
                    // coupon doesn't exist
                    else {
                        $this->_getSession()->addError(
                            $this->__('Coupon code "%s" does not exist.', Mage::helper('core')->htmlEscape($couponCode))
                        );
                    }
                }
            } else {
                $this->_getSession()->addSuccess($this->__('Coupon code was canceled.'));
            }

        } catch (Mage_Core_Exception $e) {
            $this->_getSession()->addError($e->getMessage());
        } catch (Exception $e) {
            $this->_getSession()->addError($this->__('Cannot apply the coupon code.'));
            Mage::logException($e);
        }

        $this->_goBack();
        return;
    }
}
