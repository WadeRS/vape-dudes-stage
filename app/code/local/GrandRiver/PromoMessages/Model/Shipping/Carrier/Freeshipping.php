<?php

class GrandRiver_PromoMessages_Model_Shipping_Carrier_Freeshipping
    extends Mage_Shipping_Model_Carrier_Freeshipping
    implements Mage_Shipping_Model_Carrier_Interface
{
    /**
     * FreeShipping Rates Collector
     *
     * @param Mage_Shipping_Model_Rate_Request $request
     * @return Mage_Shipping_Model_Rate_Result
     */
    public function collectRates(Mage_Shipping_Model_Rate_Request $request)
    {
        if (!Mage::helper('grandriver_promomessages')->getUseSubtotalWithDiscount()) {
            return parent::collectRates($request);
        }

        if (!$this->getConfigFlag('active')) {
            return false;
        }

        $result = Mage::getModel('shipping/rate_result');

        $this->_updateFreeMethodQuote($request);

        if (($request->getFreeShipping())
            || ($request->getPackageValueWithDiscount() >= $this->getConfigData('free_shipping_subtotal'))
        ) {
            $method = Mage::getModel('shipping/rate_result_method');

            $method->setCarrier('freeshipping');
            $method->setCarrierTitle($this->getConfigData('title'));

            $method->setMethod('freeshipping');
            $method->setMethodTitle($this->getConfigData('name'));

            $method->setPrice('0.00');
            $method->setCost('0.00');

            $result->append($method);
        }

        return $result;
    }

}
