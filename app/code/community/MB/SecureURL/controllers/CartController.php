<?php
require_once 'Mage/Checkout/controllers/CartController.php';
class MB_SecureUrl_CartController extends Mage_Checkout_CartController
{
      /**
     * Delete shoping cart item action
     */
    public function deleteAction()
    {
        $id = (int) $this->getRequest()->getParam('id');
        if ($id) {
            try {
                $this->_getCart()->removeItem($id)
                  ->save();
            } catch (Exception $e) {
                $this->_getSession()->addError($this->__('Cannot remove the item.'));
                Mage::logException($e);
            }
        }
            $url = Mage::getUrl('*/*');
            if ($_SERVER['HTTPS'] == 'on') {
                  $url = str_replace('http:', 'https:', $url);
            }
        $this->_redirectReferer($url);
    }
}
?>